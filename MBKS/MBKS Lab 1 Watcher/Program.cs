﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MBKS_Lab_1_Watcher
{
    class Program
    {
        private const String publicPath = @"Public";
        static void Main(string[] args)
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = publicPath;
            watcher.NotifyFilter = NotifyFilters.LastAccess
                                   | NotifyFilters.LastWrite
                                   | NotifyFilters.FileName
                                   | NotifyFilters.DirectoryName;

            watcher.Changed += OnChanged;
            watcher.Created += OnChanged;
            watcher.Deleted += OnChanged;
            watcher.Renamed += OnRenamed;
            watcher.EnableRaisingEvents = true;

            Console.WriteLine("Press \'q\' to quit the watcher.");
            while (Console.Read() != 'q') ;
        }

        private static Boolean InProgress = false;

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            Console.WriteLine($"[{DateTime.Now}] File: " + e.Name + " " + e.ChangeType);
            try
            {
                if ((e.ChangeType == WatcherChangeTypes.Created))
                {
                    Console.WriteLine($"Copy file {e.Name}");
                    File.Copy(e.FullPath, "Leak\\" + e.Name);
                }
            }
            catch { }
        }

        private static void OnRenamed(object source, RenamedEventArgs e)
        {
            Console.WriteLine($"[{DateTime.Now}] File: {e.OldName} renamed to {e.Name}");
        }
    }
}
