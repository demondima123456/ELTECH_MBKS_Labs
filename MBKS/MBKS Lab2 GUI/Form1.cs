﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HRUC;

namespace MBKS_Lab2_GUI
{
    public partial class Form1 : Form
    {
        private List<char> charSet = "abcdefgh".ToList();

        public Form1()
        {
            InitializeComponent();
            dataGridView1.Columns.Clear();

            dataGridView1.Columns.Add(new DataGridViewTextBoxColumn() { Name = "User", AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells});
            foreach (char c in charSet)
            {
                dataGridView1.Columns.Add(new DataGridViewCheckBoxColumn()
                {
                    Name = c.ToString(),
                    Width = 25
                });
            }

            dataGridView1.CellValueChanged += (s, ea) =>
            {
                UpdateUserList();
                textBox2.Text = GetUserTextByMatrix(textBox1.Text);
            };
            comboBox1.SelectedIndexChanged+= (s,ea) => textBox2.Text = GetUserTextByMatrix(textBox1.Text);

            for (int i = 1; i < 5; i++)
            {
                int rowInd = dataGridView1.Rows.Add();
                var foo = dataGridView1.Rows[rowInd];
                foo.Cells[0].Value = $"User {i}";
                for (int j = 0; j < charSet.Count; j++)
                {
                    foo.Cells[j + 1].Value = j % i == 0;
                }

            }

        }
        void UpdateUserList()
        {
            comboBox1.Items.Clear();
            cbFrom.Items.Clear();
            cbTo.Items.Clear();
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (dataGridView1.Rows[i].Cells["User"].Value is string user)
                {
                    cbFrom.Items.Add(user);
                    cbTo.Items.Add(user);
                    comboBox1.Items.Add(user);
                }

            }
        }
        private String GetUserTextByMatrix(String allText)
        {
            String user = comboBox1.Text;
            if (String.IsNullOrEmpty(user)) return String.Empty;
            var ac = GetAC(user);

            String text = allText;
            foreach (char c in charSet)
            {
                if (!ac[c])
                    text = text.Replace(c.ToString(), "");
            }
            return text;
        }
        void SetAC(String user, Dictionary<char, Boolean> ac)
        {
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (user.Equals(dataGridView1.Rows[i].Cells["User"].Value))
                {
                    for (int j = 0; j < charSet.Count; j++)
                    {
                        dataGridView1.Rows[i].Cells[j + 1].Value = ac[charSet[j]];
                    }

                }
            }
        }
        Dictionary<Char, Boolean> GetAC(String user)
        {
            Dictionary <Char, Boolean > dict = new Dictionary<Char, bool>();
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (user.Equals(dataGridView1.Rows[i].Cells["User"].Value))
                {
                    for (int j = 1; j <= charSet.Count; j++)
                    {
                        var cell = dataGridView1.Rows[i].Cells[j] as DataGridViewCheckBoxCell;
                        if (cell.Value is bool)
                            dict[charSet[j - 1]] = ((bool) cell.Value == true);
                        else
                            dict[charSet[j - 1]] = false;
                    }
                    return dict;
                }
            }
            return dict;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox2.Text = GetUserTextByMatrix(textBox1.Text);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            var fromAC = GetAC(cbFrom.Text);
            var toAC = GetAC(cbTo.Text);

            String chars = ibChars.ValueString;

            HashSet<Char> set = new HashSet<char>();

            var granted = chars.Distinct()
                .Where(x => fromAC.ContainsKey(x) && fromAC[x]).ToList();

            for (char i = 'a'; i <= 'z'; i++)
            {
                if (granted.Contains(i))
                    toAC[i] = true;
            }

            SetAC(cbTo.Text, toAC);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (char c in inputBox1.ValueString)
            {
                if(charSet.Contains(c))
                    continue;

                charSet.Add(c);
                var column = new DataGridViewCheckBoxColumn()
                {
                    Name = c.ToString(),
                    Width = 25
                };
                dataGridView1.Columns.Add(column);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach (char c in inputBox1.ValueString)
            {
                var index = charSet.IndexOf(c);
                if (index > 0)
                {
                    charSet.Remove(c);
                    dataGridView1.Columns.RemoveAt(index + 1);
                }
            }
        }
    }
}
