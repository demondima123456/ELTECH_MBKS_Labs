﻿using System;
using System.IO;
using HRUC;

namespace MBKS
{
    public static class FileManager
    {
        public static void MoveToPublic(FileInfo fileInfo)
        {
            FileInfo outFile = new FileInfo("Public\\" + fileInfo.Name);
            MoveUsingBuffer(fileInfo, outFile);
            fileInfo.Delete();
            //fileInfo.MoveTo("Public");
        }

        private static void MoveUsingBuffer(FileInfo inFile, FileInfo outFile)
        {
            int buffSize = 2048;
            byte[] buff = new byte[buffSize];

            using (DisposableList dispList = new DisposableList())
            {
                var readStream = dispList.Add(inFile.OpenRead());
                var writeStream = dispList.Add(outFile.OpenWrite());

                int readed;
                do
                {
                    readed = readStream.Read(buff, 0, buff.Length);
                    writeStream.Write(buff, 0, readed);
                } while (readed > 0);
            }
        }

        public static void CreatePrivareFile(String filePath, String content)
        {
            using (var writer = File.CreateText(Path.Combine("Private", filePath)))
                writer.WriteLine(content);
        }
    }
}
