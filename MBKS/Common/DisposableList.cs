﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class DisposableList : IDisposable
    {
        Stack<IDisposable> _innerStack;

        public DisposableList()
        {
            _innerStack = new Stack<IDisposable>();
        }

        public T Add<T>(T item) where T : IDisposable
        {
            _innerStack.Push(item);
            return item;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    foreach (IDisposable item in _innerStack)
                    {
                        item.Dispose();
                    }
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {

            Dispose(true);
        }
        #endregion


    }
}
