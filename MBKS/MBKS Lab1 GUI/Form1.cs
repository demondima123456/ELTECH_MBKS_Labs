﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HRUC;
using MBKS;

namespace MBKS_Lab1_GUI
{
    public partial class Form1 : Form
    {
        private bool _privateChanged = true;
        public Form1()
        {
            InitializeComponent();
            lvPublic.Items.Clear();

            Timer t = new Timer();
            t.Interval = 1000;
            t.Tick += T_Tick;
            t.Start();

        }

        private void T_Tick(object sender, EventArgs e)
        {
            ListPublic();
            ListPrivate();
            if (_privateChanged)
            {
                _privateChanged = false;
                UpdateMoveToPublicList();
            }
        }

        private void ListPublic()
        {
            DirectoryInfo foo = new DirectoryInfo(ibPublic.ValueString);
            lvPublic.Items.Clear();
            foo.EnumerateFiles().ForEach(x => lvPublic.Items.Add(x.Name));
        }

        private void ListPrivate()
        {
            DirectoryInfo foo = new DirectoryInfo(ibPrivate.ValueString);
            lvPrivate.Items.Clear();
            foo.EnumerateFiles().ForEach(x => lvPrivate.Items.Add(x.Name));
        }

        private void UpdateMoveToPublicList()
        {
            clb1.Items.Clear();
            DirectoryInfo foo = new DirectoryInfo(ibPrivate.ValueString);
            clb1.Items.Clear();
            foo.EnumerateFiles().ForEach(x => clb1.Items.Add(x.Name));
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            FileManager.CreatePrivareFile(ibName.ValueString, tbContent.Text);
            _privateChanged = true;
        }

        private void btnMove_Click(object sender, EventArgs e)
        {
            foreach (var item in clb1.CheckedItems)
            {
                String path = (String) item;
                FileInfo file = new FileInfo(ibPrivate.ValueString + "\\" + path);

                FileManager.MoveToPublic(file);
                _privateChanged = true;
            }
        }
    }
}
