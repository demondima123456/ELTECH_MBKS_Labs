﻿namespace MBKS_Lab1_GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem("ghcghfhjf");
            System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem("bb");
            System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem("nnnn");
            System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem("");
            System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem("ghcghfhjf");
            System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem("bb");
            System.Windows.Forms.ListViewItem listViewItem15 = new System.Windows.Forms.ListViewItem("nnnn");
            System.Windows.Forms.ListViewItem listViewItem16 = new System.Windows.Forms.ListViewItem("");
            this.ibPublic = new HRUC.Components.InputBox();
            this.ibPrivate = new HRUC.Components.InputBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.lvPublic = new System.Windows.Forms.ListView();
            this.lvPrivate = new System.Windows.Forms.ListView();
            this.ibName = new HRUC.Components.InputBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbContent = new System.Windows.Forms.TextBox();
            this.clb1 = new System.Windows.Forms.CheckedListBox();
            this.btnMove = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // ibPublic
            // 
            this.ibPublic.LabelText = "Public path";
            this.ibPublic.Location = new System.Drawing.Point(12, 12);
            this.ibPublic.Name = "ibPublic";
            this.ibPublic.Size = new System.Drawing.Size(356, 20);
            this.ibPublic.TabIndex = 0;
            this.ibPublic.TextBoxWidth = 250;
            this.ibPublic.ValueDouble = null;
            this.ibPublic.ValueInt = null;
            this.ibPublic.ValueString = "Public";
            // 
            // ibPrivate
            // 
            this.ibPrivate.LabelText = "Private path";
            this.ibPrivate.Location = new System.Drawing.Point(12, 38);
            this.ibPrivate.Name = "ibPrivate";
            this.ibPrivate.Size = new System.Drawing.Size(356, 20);
            this.ibPrivate.TabIndex = 0;
            this.ibPrivate.TextBoxWidth = 250;
            this.ibPrivate.ValueDouble = null;
            this.ibPrivate.ValueInt = null;
            this.ibPrivate.ValueString = "Private";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 64);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(356, 287);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tbContent);
            this.tabPage1.Controls.Add(this.btnSave);
            this.tabPage1.Controls.Add(this.ibName);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(348, 261);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Create private";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnMove);
            this.tabPage2.Controls.Add(this.clb1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(348, 261);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Move to Public";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lvPublic);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(348, 261);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "LIst public";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.lvPrivate);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(348, 261);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "List private";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // lvPublic
            // 
            this.lvPublic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvPublic.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem9,
            listViewItem10,
            listViewItem11,
            listViewItem12});
            this.lvPublic.Location = new System.Drawing.Point(0, 0);
            this.lvPublic.Name = "lvPublic";
            this.lvPublic.Size = new System.Drawing.Size(348, 261);
            this.lvPublic.TabIndex = 0;
            this.lvPublic.UseCompatibleStateImageBehavior = false;
            this.lvPublic.View = System.Windows.Forms.View.List;
            // 
            // lvPrivate
            // 
            this.lvPrivate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvPrivate.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem13,
            listViewItem14,
            listViewItem15,
            listViewItem16});
            this.lvPrivate.Location = new System.Drawing.Point(0, 0);
            this.lvPrivate.Name = "lvPrivate";
            this.lvPrivate.Size = new System.Drawing.Size(348, 261);
            this.lvPrivate.TabIndex = 1;
            this.lvPrivate.UseCompatibleStateImageBehavior = false;
            this.lvPrivate.View = System.Windows.Forms.View.List;
            // 
            // ibName
            // 
            this.ibName.LabelText = "File name";
            this.ibName.Location = new System.Drawing.Point(6, 6);
            this.ibName.Name = "ibName";
            this.ibName.Size = new System.Drawing.Size(336, 20);
            this.ibName.TabIndex = 0;
            this.ibName.TextBoxWidth = 281;
            this.ibName.ValueDouble = null;
            this.ibName.ValueInt = null;
            this.ibName.ValueString = "";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(267, 232);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbContent
            // 
            this.tbContent.Location = new System.Drawing.Point(6, 32);
            this.tbContent.Multiline = true;
            this.tbContent.Name = "tbContent";
            this.tbContent.Size = new System.Drawing.Size(336, 194);
            this.tbContent.TabIndex = 2;
            // 
            // clb1
            // 
            this.clb1.FormattingEnabled = true;
            this.clb1.Location = new System.Drawing.Point(6, 6);
            this.clb1.Name = "clb1";
            this.clb1.Size = new System.Drawing.Size(339, 214);
            this.clb1.TabIndex = 0;
            // 
            // btnMove
            // 
            this.btnMove.Location = new System.Drawing.Point(267, 232);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(75, 23);
            this.btnMove.TabIndex = 1;
            this.btnMove.Text = "Move";
            this.btnMove.UseVisualStyleBackColor = true;
            this.btnMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 363);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.ibPrivate);
            this.Controls.Add(this.ibPublic);
            this.Name = "Form1";
            this.Text = "Lab1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private HRUC.Components.InputBox ibPublic;
        private HRUC.Components.InputBox ibPrivate;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListView lvPublic;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ListView lvPrivate;
        private System.Windows.Forms.TextBox tbContent;
        private System.Windows.Forms.Button btnSave;
        private HRUC.Components.InputBox ibName;
        private System.Windows.Forms.CheckedListBox clb1;
        private System.Windows.Forms.Button btnMove;
    }
}

