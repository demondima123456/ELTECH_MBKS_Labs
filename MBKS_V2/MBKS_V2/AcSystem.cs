﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace MBKS_V2
{
    [Serializable]
    public class AcSystem
    {
        [NonSerialized]
        private static readonly Lazy<AcSystem> _instance = new Lazy<AcSystem>(() =>
        {
            try
            {
                if (File.Exists("dump.bin"))
                    using (var fs = new FileStream("dump.bin", FileMode.Open))
                        return Helper.Deserialize<AcSystem>(fs);
            }
            catch { }
            return new AcSystem();
        });
        public static AcSystem Instance => _instance.Value;

        private HashSet<AccessLevel> _levels = new HashSet<AccessLevel>();
        private HashSet<AcRole> _roles = new HashSet<AcRole>();
        private List<AcDirectory> _topLevelObjs = new List<AcDirectory>();
        // MAC
        private Dictionary<ACObject, AccessLevel> _objs = new Dictionary<ACObject, AccessLevel>();
        private Dictionary<ACSubject, AccessLevel> _subjs = new Dictionary<ACSubject, AccessLevel>();
        // RBAC
        private Dictionary<ACSubject, List<AcRole>> _subj2 = new Dictionary<ACSubject, List<AcRole>>();
        private Dictionary<ACObject, List<AcRole>> _obj2 = new Dictionary<ACObject, List<AcRole>>();

        public IReadOnlyCollection<AcDirectory> TopLevelObjects => _topLevelObjs;
        public IReadOnlyCollection<ACSubject> Subjects => _subjs.Keys;
        public IReadOnlyCollection<AccessLevel> AccessLevels => _levels;
        public IReadOnlyCollection<AcRole> Roles => _roles;

        public Boolean AddSubject(ACSubject subj, AccessLevel level)
        {
            _subjs[subj] = level;
            return true;
        }
        public void RemoveSubject(ACSubject subj) => _subjs.Remove(subj);
        public AccessLevel GetSubjectLevel(ACSubject subj) => _subjs[subj];

        public void AddAccessLevel(AccessLevel level)
        {
            if (_levels.Contains(level)) _levels.Remove(level);
            _levels.Add(level);
        }

        public AccessLevel GetObjectLevel(ACObject obj) => _objs[obj];
        public List<AcRole> GetObjectRoles(ACObject obj) => _obj2[obj];


        public Boolean CreateObject(ACSubject subj, ACObject obj, AccessLevel level)
        {
            if(obj == null) return false;
            if(_subjs[subj].Level < level.Level) return false;
            switch (obj)
            {
                case AcDirectory dir:
                    Directory.CreateDirectory(dir.Name);
                    _topLevelObjs.Add(dir);
                    break;
                case ACObject file:
                    AcDirectory topLevelDir = _topLevelObjs
                        .Find(x => string.Equals(x.Name, file.Directory, 
                            StringComparison.InvariantCultureIgnoreCase));
                    if (topLevelDir == null)
                    {
                        topLevelDir = new AcDirectory(file.Directory);
                        CreateObject(subj, topLevelDir, level);
                    }

                    topLevelDir.NestedObjects.Add(file);
                    File.Create(file.Directory + "\\" + file.Name);
                    break;
            }
            _objs[obj] = level;
            _obj2[obj] = new List<AcRole>();
            return true;
        }

        public void AddRole(AcRole acRole)
        {
            _roles.Add(acRole);
        }

        public String OpenObject(ACSubject subj, ACObject obj)
        {
            if(subj == null || obj == null) return String.Empty;
            if(obj is AcDirectory) return String.Empty;

            if (_objs[obj].Level <= _subjs[subj].Level)
            {
                using (var sr = new StreamReader(obj.Path))
                {
                    String content = sr.ReadToEnd();
                    return content;
                }
            }

            return String.Empty;
        }
        public void SaveObject(ACSubject subj, ACObject obj, String content)
        {
            if (subj == null || obj == null) return;
            if (obj is AcDirectory) return;

            if (_objs[obj].Level <= _subjs[subj].Level)
            {
                using (var sw = new StreamWriter(obj.Path, false))
                {
                    sw.Write(content);
                }
            }
        }
        public Boolean MoveObject(ACSubject subj, ACObject @from, ACObject to)
        {
            if (subj == null || @from == null || to == null) return false;
            if (@from is AcDirectory) return false;

            var dir = _topLevelObjs.Find(x => x.Name.Equals(to.Directory,
                StringComparison.InvariantCultureIgnoreCase));
            if (dir == null)
            {
                dir = new AcDirectory(to.Directory);
                CreateObject(subj,dir, _objs[from]);
            }

            if (_objs[@from].Level <= _subjs[subj].Level
                && _objs[dir].Level >= _objs[@from].Level)
            {
                String newPath = to.Path;

                File.Delete(newPath);
                File.Move(@from.Path, newPath);

                var level = _objs[@from];
                var roles = _obj2[@from];
                UnregisterObject(@from);
                @from.Directory = to.Directory;
                RegisterObject(@from, level, roles);
                return true;
            }

            return false;
        }
        public void CopyObject(ACSubject subj, ACObject obj, AcDirectory dir)
        {
            if (subj == null || obj == null || dir == null) return;
            if (obj is AcDirectory) return;

            if (_objs[obj].Level <= _subjs[subj].Level &&
                _objs[dir].Level >= _objs[obj].Level)
            {
                String newPath = dir.Name + "\\" + obj.Name;
                File.Copy(obj.Path, newPath, true);

                ACObject newObj = new ACObject(obj.Name, dir.Name);
                RegisterObject(newObj, _objs[obj], _obj2[obj]);

            }
        }
        public void DeleteObject(ACSubject subj, ACObject obj)
        {
            if (subj == null || obj == null) return;

            if (_objs[obj].Level <= _subjs[subj].Level)
            {
                if (obj is AcDirectory)
                {
                    Directory.Delete(obj.Name);
                }
                else
                {
                    File.Delete(obj.Path);
                }

                UnregisterObject(obj);

            }
        }

        public void AssignRole(ACSubject subj, params AcRole[] roles) => _subj2[subj] = roles.ToList();
        public void AssignRole(ACObject obj, params AcRole[] roles) => _obj2[obj] = roles.ToList();

        public IReadOnlyCollection<AcDirectory> GetAvalibleObjectsByMac(ACSubject subj)
        {
            if (subj == null) return null;
            var avaliableByMac = _topLevelObjs
                .Where(x =>  _objs[x].Level <= _subjs[subj].Level)
                .ToList();

            List<AcDirectory> res = new List<AcDirectory>();

            foreach (AcDirectory dic in avaliableByMac)
            {
                AcDirectory clone = new AcDirectory(dic.Name);
                clone.NestedObjects.Clear();
                clone.NestedObjects.AddRange(dic.NestedObjects.Where(x => _objs[x].Level <= _subjs[subj].Level));
                res.Add(clone);
            }

            return res;
        }

        public IReadOnlyCollection<AcDirectory> GetAvalibleObjectsByRoles(ACSubject subj)
        {
            if (subj == null) return null;
            var avaliableByMac = GetAvalibleObjectsByMac(subj);
            List<AcDirectory> res = new List<AcDirectory>();

            if (_subj2.ContainsKey(subj))
            {
                var subjRoles = _subj2[subj];
                foreach (AcDirectory dic in avaliableByMac)
                {
                    if(_obj2.ContainsKey(dic))
                    if (_obj2[dic].Any(x => subjRoles.Contains(x)))
                    {
                        Boolean validateRole(ACObject obj, List<AcRole> subjectRoles)
                        {
                            if (_obj2.TryGetValue(obj, out List<AcRole> objRoles))
                                return objRoles.Any(x => subjectRoles.Contains(x));
                            return false;
                        }

                        AcDirectory clone = new AcDirectory(dic.Name);
                        clone.NestedObjects.Clear();
                        clone.NestedObjects.AddRange(dic.NestedObjects.Where(x =>
                            _objs[x].Level <= _subjs[subj].Level
                            && validateRole(x, subjRoles)));
                        res.Add(clone);
                    }
                }
            }

            return res;
        }

        private void RegisterObject(ACObject obj, AccessLevel level, List<AcRole> roles)
        {
            _topLevelObjs.Find(x => x.Name.Equals(obj.Directory))
                .NestedObjects.Add(obj);
            _objs[obj] = level;
            _obj2[obj] = roles;
        }
        private void UnregisterObject(ACObject obj)
        {
            _topLevelObjs.Find(x => x.Name.Equals(obj.Directory))
                .NestedObjects.Remove(obj);
            _objs.Remove(obj);
            _obj2.Remove(obj);
        }
    }
}