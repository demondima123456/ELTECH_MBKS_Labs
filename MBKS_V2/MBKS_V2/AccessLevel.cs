﻿using System;
using System.Collections.Generic;

namespace MBKS_V2
{
    [Serializable]
    public class AccessLevel : IEquatable<AccessLevel>
    {
        public AccessLevel(int level, string name)
        {
            Level = level;
            Name = name;
        }

        public Int32 Level { get; private set; }
        public String Name { get; private set; }

        public static AccessLevel Public => new AccessLevel(0, "Public");
        public static AccessLevel Secret => new AccessLevel(10, "Secret");
        public static AccessLevel TopSecret => new AccessLevel(255, "TopSecret");

        #region Equals and GetHashCode
        public override bool Equals(object obj)
        {
            return Equals(obj as AccessLevel);
        }
        public bool Equals(AccessLevel other)
        {
            return other != null &&
                   //Level == other.Level &&
                   Name == other.Name;
        }
        public override int GetHashCode()
        {
            var hashCode = 616777995;
            //hashCode = hashCode * -1521134295 + Level.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            return hashCode;
        }
        public static bool operator ==(AccessLevel level1, AccessLevel level2)
        {
            return EqualityComparer<AccessLevel>.Default.Equals(level1, level2);
        }
        public static bool operator !=(AccessLevel level1, AccessLevel level2)
        {
            return !(level1 == level2);
        }
        #endregion
    }
}