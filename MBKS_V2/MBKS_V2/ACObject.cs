﻿using System;

namespace MBKS_V2
{
    [Serializable]
    public class ACObject
    {
        public ACObject(string name, string dir)
        {
            Name = name;
            Directory = dir;
        }
        public string Name { get; set; }
        public string Directory { get; set; }

        public String Path => Directory + "\\" + Name;
        public override String ToString() => Name;
        public String Level => "[" + AcSystem.Instance.GetObjectLevel(this).Name + "] ";

        protected bool Equals(ACObject other)
        {
            return string.Equals(Name, other.Name) && string.Equals(Directory, other.Directory);
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ACObject) obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return ((Name != null ? Name.GetHashCode() : 0) * 397) ^ (Directory != null ? Directory.GetHashCode() : 0);
            }
        }
    }
}