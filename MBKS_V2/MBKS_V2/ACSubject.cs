﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace MBKS_V2
{
    [Serializable]
    public class ACSubject : IEquatable<ACSubject>
    {
        public ACSubject(string name)
        {
            Name = name;
        }

        public String Name { get; set; }

        public String Level => "[" + AcSystem.Instance.GetSubjectLevel(this).Name + "] ";

        public override bool Equals(object obj)
        {
            return Equals(obj as ACSubject);
        }
        public bool Equals(ACSubject other)
        {
            return other != null &&
                   string.Equals(Name, other.Name, StringComparison.InvariantCultureIgnoreCase);
        }
        public override int GetHashCode()
        {
            return 539060726 + EqualityComparer<string>.Default.GetHashCode(Name.ToLower(CultureInfo.InvariantCulture));
        }
    }
}