﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBKS_V2
{
    [Serializable]
    public class AcDirectory : ACObject
    {
        public AcDirectory(String name) : base(name, null)
        {
            NestedObjects = new List<ACObject>();
        }
        public List<ACObject> NestedObjects { get; private set; }

        public new String Level => "[" + AcSystem.Instance.GetObjectLevel(this).Name + "] ";

        public String Roles => "(" + AcSystem.Instance.GetObjectRoles(this)
                                   .Aggregate(" ", (st,r) => st+r.Name+" ") + ") ";
    
    }


}
