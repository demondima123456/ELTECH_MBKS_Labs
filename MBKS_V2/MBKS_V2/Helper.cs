﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MBKS_V2
{
    public static class Helper
    {
        public static void DumpOnDisk(this AcSystem system)
        {
            using (var fs = new FileStream("dump.bin", FileMode.Create))
                Serialize(system, fs);
            
        }

        public static void Serialize<Object>(Object dictionary, Stream stream)
        {
            try
            {
                using (stream)
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(stream, dictionary);
                }
            }
            catch (IOException)
            {
            }
        }
        public static Object Deserialize<Object>(Stream stream) where Object : new()
        {
            Object ret = CreateInstance<Object>();
            try
            {
                using (stream)
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    ret = (Object)bin.Deserialize(stream);
                }
            }
            catch (IOException)
            {
            }
            return ret;
        }
        public static Object CreateInstance<Object>() where Object : new()
        {
            return (Object)Activator.CreateInstance(typeof(Object));
        }
    }
}
