﻿using System;

namespace MBKS_V2
{
    [Serializable]
    public class AcRole
    {
        public AcRole(string name/*, AccessLevel[] level*/)
        {
            Name = name;
            //Levels = level;
        }

        public string Name { get; set; }
        //public AccessLevel[] Levels { get; set; }
    }
}