﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using MBKS_UI.ViewModel;
using MBKS_V2;

namespace MBKS_UI
{
    /////////////////////////////////////////////
    /////////////////////////////////////////////
    public class ParseCommand : ICommand
    {
        public bool CanExecute(object parameter) => true;
        public void Execute(object parameter)
        {
            var vm = (AccessControlSystemViewModel) parameter;

            CommandParses.Subject = vm.SelectedUser;

            var logLIne = CommandParses.Parse(vm.CmdLine);
            vm.AppendLog(logLIne);
            vm.RaisePropertyChanged();
        }

        public event EventHandler CanExecuteChanged;
    }

}