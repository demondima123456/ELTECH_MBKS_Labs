﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MBKS_V2;

namespace MBKS_UI
{
    public static class CommandParses
    {
        public static ACSubject Subject { get; set; }
        public static String Parse(String cmd)
        {
            try
            {
                var terms = cmd.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                Int32 next = 0;
                switch (terms[next++])
                {
                    case "add": return ParseAdd(terms, next);
                    case "move": return ParseMove(terms, next);
                    case "del": return ParseDel(terms, next);
                    case "assign": return ParseAssign(terms, next);
                    case "dump": return ParseDump();
                    default: throw new InvalidCastException(nameof(cmd));
                }
            }
            catch
            {
                return "Error";
            }
        }

        private static string ParseDump()
        {
            AcSystem.Instance.DumpOnDisk();
            return "Dump completed";
        }

        private static string ParseAdd(string[] terms, Int32 next)
        {
            switch (terms[next++])
            {
                case "user": return ParseAddUser(terms, next);
                case "level": return ParseAddLevel(terms, next);
                case "dir": return ParseAddDir(terms, next);
                case "file": return ParseAddFile(terms, next);
                case "role": return ParseAddRole(terms, next);
            }

            throw new Exception();
        }

        private static string ParseAssign(string[] terms, int next)
        {
            switch (terms[next++])
            {
                case "role2obj": return ParseAssignRole2Obj(terms, next);
                case "role2subj": return ParseAssignRole2Subj(terms, next);
            }

            throw new Exception();
        }

        private static string ParseAssignRole2Obj(string[] terms, int next)
        {
            var obj = ParseObj(terms[next++]);
            var roles = ParseRoles(terms[next++]);

            AcSystem.Instance.AssignRole(obj, roles);

            return "Sucsessfuly assign roles";
        }

        private static string ParseAssignRole2Subj(string[] terms, int next)
        {
            var subj = terms[next++];
            var roles = ParseRoles(terms[next++]);

            AcSystem.Instance.AssignRole(new ACSubject(subj), roles);

            return "Sucsessfuly assign roles";
        }

        private static string ParseAddLevel(string[] terms, int next)
        {
            String name = terms[next++];
            Int32 level = Int32.Parse(terms[next++]);

            AcSystem.Instance.AddAccessLevel(new AccessLevel(level, name));

            return "New access level added.";
        }
        private static string ParseAddUser(string[] terms, int next)
        {
            String name = terms[next++];
            String levelName = terms[next++];

            var level = AcSystem.Instance.AccessLevels.Single(x => x.Name.Equals(levelName, StringComparison.InvariantCultureIgnoreCase));
            AcSystem.Instance.AddSubject(new ACSubject(name), level);

            return "New user added.";
        }
        private static string ParseAddFile(string[] terms, int next)
        {
            String target = terms[next++];
            String levelName = terms[next++];

            var obj = ParseObj(target);

            var level = AcSystem.Instance.AccessLevels.Single(x =>
                x.Name.Equals(levelName, StringComparison.InvariantCultureIgnoreCase));

            if(AcSystem.Instance.CreateObject(Subject, obj, level))
                return "New file added.";
            return "Eror while adding file.";
        }
        private static string ParseAddDir(string[] terms, int next)
        {
            String name = terms[next++];
            String levelName = terms[next++];

            var level = AcSystem.Instance.AccessLevels.Single(x =>
                x.Name.Equals(levelName, StringComparison.InvariantCultureIgnoreCase));

            if(AcSystem.Instance.CreateObject(Subject, new AcDirectory(name), level))
                return "New directory added.";
            return "Eror while adding directory.";
        }

        private static string ParseAddRole(string[] terms, int next)
        {
            String name = terms[next++];
            //AccessLevel[] levels = ParseLevels(terms[next++]);
            AcSystem.Instance.AddRole(new AcRole(name/*, levels*/));

            return "New role added.";
        }

        private static string ParseMove(string[] terms, int next)
        {
            var from = ParseObj(terms[next++]);
            var to = ParseObj(terms[next++]);

            if(AcSystem.Instance.MoveObject(Subject, from, to))
                return "Move completed";

            return "Error while moving (invalid access)";
        }

        private static string ParseDel(string[] terms, int next)
        {
            var obj = ParseObj(terms[next++]);

            AcSystem.Instance.DeleteObject(Subject, obj);

            return "Delete completed";
        }

        private static ACObject ParseObj(string term)
        {
            var parts = term.Split(new[] {'\\', '/'}, StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length == 1)
                return new AcDirectory(parts[0]);
            else if (parts.Length == 2)
                return new ACObject(parts[1], parts[0]);

            throw new Exception("ParsingError");
        }

        private static AcRole[] ParseRoles(string term)
        {
            var arr = term.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            return AcSystem.Instance
                .Roles
                .Where(x => arr.Contains(x.Name, StringComparer.InvariantCultureIgnoreCase))
                .ToArray();
            
        }

        private static AccessLevel[] ParseLevels(string term)
        {
            var arr = term.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            return arr.Select(levelName =>
                AcSystem.Instance.AccessLevels.Single(x =>
                    x.Name.Equals(levelName, 
                        StringComparison.InvariantCultureIgnoreCase)))
                .ToArray();
        }
    }
}
