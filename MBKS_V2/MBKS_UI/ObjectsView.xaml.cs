﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MBKS_UI.ViewModel;
using MBKS_V2;

namespace MBKS_UI
{
    /// <summary>
    /// Interaction logic for ObjectsView.xaml
    /// </summary>
    public partial class ObjectsView : UserControl
    {
        public ObjectsView()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(100);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if ((DataContext as ObjectListViewModel)?.NeedToUpdate ?? false)
            {
                BindingExpression binding = ObjTree.GetBindingExpression(TreeView.ItemsSourceProperty);
                binding?.UpdateTarget();
            }
        }

        private void ObjTree_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            ((ObjectListViewModel) DataContext).SelectedObject = (ACObject)e.NewValue;
        }

        private void ObjTree_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F2)
            {
                BindingExpression binding = ObjTree.GetBindingExpression(TreeView.ItemsSourceProperty);
                binding?.UpdateTarget();
            }
        }
    }
}
