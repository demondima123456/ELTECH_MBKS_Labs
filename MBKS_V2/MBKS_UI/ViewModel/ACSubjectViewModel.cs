﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using MBKS_UI.Annotations;
using MBKS_V2;

namespace MBKS_UI.ViewModel
{
    public class ACSubjectViewModel: INotifyPropertyChanged
    {
        private ACSubject _selectedUser;

        public IReadOnlyCollection<ACSubject> Users => AcSystem.Instance.Subjects;
        public IReadOnlyCollection<AccessLevel> AccessLevels => AcSystem.Instance.AccessLevels;

        public String UserToAddName { get; set; }
        public AccessLevel UserToAddAccessLevel { get; set; }

        public ACSubject SelectedUser
        {
            get => _selectedUser;
            set
            {
                _selectedUser = value;
                OnPropertyChanged(nameof(SelectedUser));
            }
        }

        public ICommand AddUser { get; } = new AddUserCommand();
        public ICommand DelUser { get; }= new DelUserCommand();

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
