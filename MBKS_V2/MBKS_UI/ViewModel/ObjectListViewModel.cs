﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MBKS_UI.Annotations;
using MBKS_V2;

namespace MBKS_UI.ViewModel
{
    public class ObjectListViewModel: INotifyPropertyChanged
    {
        public ACSubject SelectedSubject
        {
            get => _selectedSubject;
            set
            {
                if (value != _selectedSubject)
                {
                    NeedToUpdate = true;
                    _selectedSubject = value;
                    OnPropertyChanged(nameof(SelectedSubject));
                }
            }
        }

        public IReadOnlyCollection<AcDirectory> Directories =>
            AcSystem.Instance.GetAvalibleObjectsByMac(SelectedSubject);

        private bool _needToUpdate;
        private ACSubject _selectedSubject = null;
        private ACObject _selectedObject;

        public bool NeedToUpdate
        {
            get
            {
                if (_needToUpdate)
                {
                    _needToUpdate = false;
                    return true;
                }

                return false;
            }
            set { _needToUpdate = value; }
        }

        public ACObject SelectedObject
        {
            get => _selectedObject;
            set
            {
                if (value != _selectedObject)
                {
                    _selectedObject = value;
                    OnPropertyChanged(nameof(SelectedObject));
                }
            }
        
        }

        public ACObject PrepareToCopyObject { get; set; }

        public ICommand PrepareToCopyCommand { get ; } = new PrepareToCopyCommand();
        public ICommand CopyCommand { get; } = new CopyCommand();
        public ICommand MoveCommand { get; } = new MoveCommand();
        public ICommand DelCommand { get; } = new DelSubjCommand();



        /*
         * public object CopyCommand
        {
            get { throw new NotImplementedException(); }
        }

        public object PasteCommand
        {
            get { throw new NotImplementedException(); }
        }
         */
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
