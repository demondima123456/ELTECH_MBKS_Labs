﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MBKS_UI.Annotations;
using MBKS_UI.ViewModel;
using MBKS_V2;

namespace MBKS_UI.ViewModel
{
    public class AccessControlSystemViewModel: INotifyPropertyChanged
    {
        public ObservableCollection<ACSubject> Users => new ObservableCollection<ACSubject>(AcSystem.Instance.Subjects);
        public ObservableCollection<AccessLevel> AccessLevels => new ObservableCollection<AccessLevel>(AcSystem.Instance.AccessLevels);
        public ObservableCollection<AcRole> Roles => new ObservableCollection<AcRole>(AcSystem.Instance.Roles);

        public IReadOnlyCollection<AcDirectory> Directories
        {
            get
            {
                if (RoleEnable)
                   return AcSystem.Instance.GetAvalibleObjectsByRoles(SelectedUser);
                return AcSystem.Instance.GetAvalibleObjectsByMac(SelectedUser);
            }
        }

        private ACSubject _selectedUser;
        public ACSubject SelectedUser
        {
            get => _selectedUser;
            set
            {
                _selectedUser = value;
                OnPropertyChanged(nameof(SelectedUser));
                OnPropertyChanged(nameof(Directories));
            }
        }

        public ICommand ParseCommand { get; } = new ParseCommand();
        public String CmdLine { get; set; }

        public ObservableCollection<String> Logs { get; set; } = new ObservableCollection<string>();

        public Boolean RoleEnable { get; set; }

        public void AppendLog(String logline)
        {
            Logs.Add(logline);
        }

        public void RaisePropertyChanged()
        {
            OnPropertyChanged(nameof(AccessLevels));
            OnPropertyChanged(nameof(Users));
            OnPropertyChanged(nameof(Directories));
            OnPropertyChanged(nameof(Roles));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

}
