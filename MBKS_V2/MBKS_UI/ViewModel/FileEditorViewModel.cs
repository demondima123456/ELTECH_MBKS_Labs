﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MBKS_UI.Annotations;
using MBKS_V2;

namespace MBKS_UI.ViewModel
{
    public class FileEditorViewModel: INotifyPropertyChanged
    {
        private string _content;
        private ACSubject _selectedSubject;
        private ACObject _selectedObject;
        private string _path;

        public IReadOnlyCollection<AccessLevel> AccessLevels => AcSystem.Instance.AccessLevels;

        public ACSubject SelectedSubject
        {
            get => _selectedSubject;
            set
            {
                if (_selectedSubject != value)
                {
                    _selectedSubject = value;
                    OnPropertyChanged(nameof(SelectedSubject));
                }
            }
        }
        public ACObject SelectedObject
        {
            get => _selectedObject;
            set
            {
                if (_selectedObject != value)
                {
                    _selectedObject = value;
                    OnPropertyChanged(nameof(SelectedObject));
                }
            }
        }

        public String Content { get; set; }

        public ICommand SaveCommand { get; } = new SaveCommand();
        public ICommand CreateCommand { get; } = new CreateCommand();

        public String Path
        {
            get => _path;
            set
            {
                _path = value;
                OnPropertyChanged(nameof(Path));
            }
        }


        public void UpdateContent()
        {
            Content = AcSystem.Instance.OpenObject(SelectedSubject, SelectedObject);
            OnPropertyChanged(nameof(Content));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
